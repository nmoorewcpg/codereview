﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;

namespace CustomerPortal.Entities
{
    public class Product
    {
        public virtual Guid ID { get; set; }
        public virtual string SKU { get; set; }
        public virtual string Size { get; set; }
        public virtual string CustomerSKU { get; set; }
        public virtual string CustomerNumber { get; set; }
        public virtual string BasePartNumber { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string Applications { get; set; }
        public virtual string MarketingCopy { get; set; }
        public virtual string UnitOfMeasure { get; set; }
        public virtual string Availability { get; set; }
        public virtual double Price
        {
            get
            {
                if (CustomerSpecificPrice > 0.0)
                    return CustomerSpecificPrice;
                return PriceListPrice;
            }
        }
        public virtual double PriceListPrice { get; set; }
        public virtual double CustomerSpecificPrice { get; set; }
        public virtual double Discount { get; set; }
        public virtual double DiscountedPrice
        {
            get
            {
                if (Discount > 0)
                    return Math.Round(Price - (Price * Discount), 2);
                return Price;
            }
        }
        public virtual string PriceList { get; set; }
        public virtual string Photo { get; set; }
        public virtual string NonNullDescription { get { return this.Description ?? ""; } set { } }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public sealed class ProductMapping : ClassMap<Product>
    {
        public ProductMapping()
        {
            ReadOnly();
            LazyLoad();
            Cache.ReadOnly();
            Table("PortalItem");
            CompositeId()
                .KeyProperty(x => x.SKU)
                .KeyProperty(x => x.CustomerNumber);
            Map(p => p.Size);
            Map(p => p.CustomerSKU);
            Map(p => p.BasePartNumber);
            Map(p => p.Name);
            Map(p => p.Description);
            Map(p => p.MarketingCopy);
            Map(p => p.UnitOfMeasure);
            Map(p => p.CustomerSpecificPrice);
            Map(p => p.PriceList);
            Map(p => p.PriceListPrice);
            Map(p => p.Photo);
            Map(p => p.Discount);
        }
    }
}