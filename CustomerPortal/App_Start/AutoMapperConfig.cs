﻿using System.Linq;
using AutoMapper;
using CustomerPortal.Entities;
using CustomerPortal.Models;
using SAP.Entities;

public static class AutoMapperConfig
{
    public static IMapper LoadMapper()
    {
        var config = new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<Invoice, InvoiceListItemViewModel>();
            cfg.CreateMap<Order, OrderListItemViewModel>();
            cfg.CreateMap<Product, ProductListItemViewModel>()
                .ForMember("URL", opt => opt.MapFrom(x => @"http://westchestergear.com/sites/default/files/product_images/" + x.BasePartNumber + ".png"));
        });

        return config.CreateMapper();
    }
}