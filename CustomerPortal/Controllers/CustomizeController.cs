﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ImageMagick;

namespace CustomerPortal.Controllers
{
    public class CustomizeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Index2()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult ImageList()
        {
            var files = new List<string>();
            var dir = new DirectoryInfo(Server.MapPath("~/Content/Uploads/"));

            foreach (var file in dir.GetFiles())
                files.Add("/Content/Uploads/" + file.Name);
            return PartialView(files);
        }

        [HttpPost]
        public ActionResult UploadImage(HttpPostedFileBase image)
        {
            //you can put your existing save code here
            if (image != null && image.ContentLength > 0)
            {
                var tempName = Guid.NewGuid().ToString();

                var ext = Path.GetExtension(image.FileName);
                if (ext == ".eps" || ext == ".pdf" || ext == ".svg")
                {
                    using (MagickImageCollection images = new MagickImageCollection())
                    {
                        images.Read(image.InputStream);

                        int page = 1;
                        foreach (MagickImage mImage in images)
                        {
                            var path = Path.Combine(Server.MapPath("~/Content/Uploads/"));

                            // Write page to file that contains the page number
                            mImage.Write(path + tempName + ".png");
                            page++;
                        }
                    }
                    var vectorPath = Path.Combine(Server.MapPath("~/Content/Uploads/Vector"),
                                             tempName + ext);
                    image.SaveAs(vectorPath);
                }
                else
                {
                    var path = Path.Combine(Server.MapPath("~/Content/Uploads/"),
                                            System.IO.Path.GetFileName(image.FileName));
                    image.SaveAs(path);
                }
            }

            return RedirectToAction("ImageList");
        }

        [HttpPost]
        public string PlaceOrder()
        {
            var orderNumber = "NICK";// OrderService.CreateOrder();
            Directory.CreateDirectory((Server.MapPath("~/Content/Temp/") + orderNumber));
            return orderNumber;
        }

        [HttpPost]
        public string FlattenImage(FlattenModel model)
        {
            string imgBase = "";
            if (model.Area == "front")
                imgBase = Path.Combine(Server.MapPath("~/img/"), "Flat-47205-Front.png");
            else
                imgBase = Path.Combine(Server.MapPath("~/img/"), "Flat-47205-Back.png");

            string uploadDir = Server.MapPath("~/Content/Uploads/");
            string vectorUploadDir = Server.MapPath("~/Content/Uploads/Vector");
            string tempDir = Path.Combine(Server.MapPath("~/Content/Temp/"), model.OrderNumber, model.Area);
            string artworkDir = Path.Combine(tempDir, "artwork");

            Directory.CreateDirectory(tempDir);
            Directory.CreateDirectory(artworkDir);

            using (MagickImage image = new MagickImage(imgBase))
            {
                image.Scale(530, 530);

                using (Bitmap imageObj = image.ToBitmap())
                {
                    foreach (var canvasImage in model.canvasImages)
                    {
                        string path = Path.Combine(uploadDir, Path.GetFileName(canvasImage.Source));
                        using (MagickImage overlayImage = new MagickImage(path))
                        {
                            overlayImage.Scale(Convert.ToInt32(canvasImage.Width), Convert.ToInt32(canvasImage.Height));

                            using (Bitmap watermarkObj = overlayImage.ToBitmap())
                            {
                                using (Graphics imageGraphics = Graphics.FromImage(imageObj))
                                {
                                    var trueLeft = Convert.ToInt32(canvasImage.Left) - (overlayImage.Width / 2);
                                    var trueTop = Convert.ToInt32(canvasImage.Top) - (overlayImage.Height / 2);

                                    Point point = new Point(trueLeft, trueTop);
                                    imageGraphics.DrawImage(watermarkObj, point);
                                }
                            }
                        }

                        System.IO.File.Copy(path, Path.Combine(artworkDir, Path.GetFileName(canvasImage.Source)));

                        string[] files = System.IO.Directory.GetFiles(vectorUploadDir, Path.GetFileNameWithoutExtension(canvasImage.Source) + ".*", System.IO.SearchOption.TopDirectoryOnly);
                        if (files.Length > 0)
                        {
                            string vectorDir = Path.Combine(artworkDir, "vector");
                            Directory.CreateDirectory(vectorDir);
                            System.IO.File.Copy(files[0], Path.Combine(vectorDir, Path.GetFileName(files[0])));
                        }
                    }

                    var saveName = Guid.NewGuid().ToString() + ".png";
                    var savePath = Path.Combine(Server.MapPath("~/Content/Temp/"), model.OrderNumber, model.Area, saveName);
                    imageObj.Save(savePath);
                    return saveName;
                }
            }
        }

        [HttpGet]
        public virtual ActionResult Download(string file)
        {
            string fullPath = Path.Combine(Server.MapPath("~/Content/Temp/"), file);
            return File(fullPath, "image/jpeg", file);
        }

        [HttpPost]
        public void EmailSomething(string orderNumber)
        {
            //EmailService.SendMessage("A Customization Quote Request Has Been Placed", string.Format("Order Number:{0}\n\nFile Location: {1}{0}", orderNumber, @"\\fred\temp\"), "dmurphy@westchestergear.com");
        }
    }

    public class CanvasImageModel
    {
        public double Top { get; set; }
        public double Left { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public string Source { get; set; }
    }

    public class FlattenModel
    {
        public string Area { get; set; }
        public string OrderNumber { get; set; }
        public CanvasImageModel[] canvasImages { get; set; }
    }
}