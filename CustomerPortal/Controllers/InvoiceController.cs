﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using CustomerPortal.Models;
using Ninject;
using SAP.Entities;

namespace CustomerPortal.Controllers
{
    public class InvoiceController : _ApplicationController
    {
        [Inject]
        public IRepository<Invoice, SAPDatabaseContext> invoiceRepo { get; set; }


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadInvoices(DataTableParamModel param)
        {
            var allInvoices = invoiceRepo.GetGroup(x => x.CustomerNumber == "LY02");
            IEnumerable<Invoice> filteredInvoices;

            if (!string.IsNullOrEmpty(param.sSearch))
            {
                filteredInvoices = allInvoices.AsEnumerable()
                         .Where(x => x.InvoiceNumber.Contains(param.sSearch) ||
                         x.PONumber.Contains(param.sSearch) ||
                         x.Status.Contains(param.sSearch) ||
                         x.InvoiceDate.ToString().Contains(param.sSearch) ||
                         x.InvoiceAmount.ToString().Contains(param.sSearch));
            }
            else
            {
                filteredInvoices = allInvoices;
            }

            var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

            Func<Invoice, string> orderingFunction =
                (x => sortColumnIndex == 0 ? x.InvoiceNumber :
                sortColumnIndex == 1 ? x.PONumber :
                sortColumnIndex == 2 ? x.Status :
                sortColumnIndex == 3 ? x.InvoiceDateSAP : x.InvoiceAmount.ToString());

            var sortDirection = Request["sSortDir_0"]; // asc or desc
            if (sortDirection == "asc")
                filteredInvoices = filteredInvoices.OrderBy(orderingFunction);
            else
                filteredInvoices = filteredInvoices.OrderByDescending(orderingFunction);

            var displayedInvoices = filteredInvoices
                .Skip(param.iDisplayStart)
                .Take(param.iDisplayLength);

            var result = Mapper.Map<IEnumerable<InvoiceListItemViewModel>>(displayedInvoices);

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = allInvoices.Count(),
                iTotalDisplayRecords = filteredInvoices.Count(),
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }
    }
}