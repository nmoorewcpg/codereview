﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using CustomerPortal.Models;
using Ninject;
using SAP.Entities;

namespace CustomerPortal.Controllers
{
    public class OrderController : _ApplicationController
    {
        [Inject]
        public IRepository<Order, SAPDatabaseContext> orderRepo { get; set; }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadOrders(DataTableParamModel param)
        {
            var allOrders = orderRepo.GetGroup(x => x.SoldTo == "LY02");
            IEnumerable<Order> filteredOrders;

            if (!string.IsNullOrEmpty(param.sSearch))
            {
                string searchTerm = param.sSearch.ToLower();
                filteredOrders = allOrders.AsEnumerable()
                         .Where(x => x.SalesOrder.Contains(searchTerm) ||
                         x.PurchaseOrder.ToLower().Contains(searchTerm) ||
                         x.ShipName.ToLower().Contains(searchTerm) ||
                         x.Status.ToLower().Contains(searchTerm) ||
                         x.OrderDate.ToString().Contains(searchTerm) ||
                         x.OrderValue.ToString().Contains(searchTerm));
            }
            else
            {
                filteredOrders = allOrders;
            }

            var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

            Func<Order, string> orderingFunction =
                (x => sortColumnIndex == 0 ? x.SalesOrder :
                sortColumnIndex == 1 ? x.PurchaseOrder :
                sortColumnIndex == 2 ? x.ShipName :
                sortColumnIndex == 3 ? x.StatusCode :
                sortColumnIndex == 4 ? x.OrderDateSAP : x.OrderValue.ToString());

            var sortDirection = Request["sSortDir_0"]; // asc or desc
            if (sortDirection == "asc")
                filteredOrders = filteredOrders.OrderBy(orderingFunction);
            else
                filteredOrders = filteredOrders.OrderByDescending(orderingFunction);

            var displayedOrders = filteredOrders
                .Skip(param.iDisplayStart)
                .Take(param.iDisplayLength);

            var result = Mapper.Map<IEnumerable<OrderListItemViewModel>>(displayedOrders);

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = allOrders.Count(),
                iTotalDisplayRecords = filteredOrders.Count(),
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }
    }
}