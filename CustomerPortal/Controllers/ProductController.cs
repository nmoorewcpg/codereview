﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using CustomerPortal.Entities;
using CustomerPortal.Models;
using Ninject;
using SAP.Entities;

namespace CustomerPortal.Controllers
{
    public class ProductController : _ApplicationController
    {
        [Inject]
        public IRepository<Product, CustomerPortalDatabaseContext> productRepo { get; set; }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadProducts(DataTableParamModel param)
        {
            var allProducts = productRepo.GetGroup(x => x.CustomerNumber == "LY02");
            IEnumerable<Product> filteredProducts;

            if (!string.IsNullOrEmpty(param.sSearch))
            {
                filteredProducts = allProducts
                         .Where(x => x.Name.Contains(param.sSearch) ||
                         x.SKU.Contains(param.sSearch) ||
                         x.CustomerSKU.Contains(param.sSearch) ||
                         x.Description.Contains(param.sSearch));
            }
            else
            {
                filteredProducts = allProducts;
            }

            var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

            Func<Product, string> orderingFunction = 
                (x => sortColumnIndex == 1 ? x.SKU :
                sortColumnIndex == 2 ? x.CustomerSKU :
                sortColumnIndex == 3 ? x.Description :
                sortColumnIndex == 4 ? x.Size : x.Price.ToString());

            var sortDirection = Request["sSortDir_0"]; // asc or desc
            if (sortDirection == "asc")
                filteredProducts = filteredProducts.OrderBy(orderingFunction);
            else
                filteredProducts = filteredProducts.OrderByDescending(orderingFunction);

            var displayedProducts = filteredProducts
                .Skip(param.iDisplayStart)
                .Take(param.iDisplayLength);

            var result = Mapper.Map<List<ProductListItemViewModel>>(displayedProducts);

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = allProducts.Count(),
                iTotalDisplayRecords = filteredProducts.Count(),
                aaData = result
            }, JsonRequestBehavior.AllowGet);
        }
    }
}