﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Ninject;
using SAP.Entities;

namespace CustomerPortal.Controllers
{
    public class _ApplicationController : Controller
    {
        [Inject]
        public IMapper Mapper { get; set; }
    }
}