﻿using System.Reflection;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Context;

namespace CustomerPortal
{
    public interface DatabaseContext
    {
        ISessionFactory GetSessionFactory();
    }

    public class CustomerPortalDatabaseContext : DatabaseContext
    {
        public ISessionFactory GetSessionFactory()
        {

            return Fluently.Configure()
                          .Database(MsSqlConfiguration.MsSql2000.ShowSql().ConnectionString(c => c.FromConnectionStringWithKey("CustomerPortalConnection")))
                          .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("CustomerPortal.Entities")))
                          .ExposeConfiguration(x => x.SetInterceptor(new SqlStatementInterceptor()))
                          .CurrentSessionContext<WebSessionContext>()
                          .BuildSessionFactory();
        }
    }

    public class SAPDatabaseContext : DatabaseContext
    {
        public ISessionFactory GetSessionFactory()
        {

            return Fluently.Configure()
                          .Database(MsSqlConfiguration.MsSql2000.ShowSql().ConnectionString(c => c.FromConnectionStringWithKey("SAPConnection")))
                          .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("SAP.Entities")))
                          .ExposeConfiguration(x => x.SetInterceptor(new SqlStatementInterceptor()))
                          .CurrentSessionContext<WebSessionContext>()
                          .BuildSessionFactory();
        }
    }
}