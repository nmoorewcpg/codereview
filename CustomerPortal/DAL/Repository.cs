﻿using System;
using System.Linq;
using System.Linq.Expressions;
using NHibernate;
using NHibernate.Linq;

namespace CustomerPortal
{
    public interface IRepository<TEntity, TContext> where TEntity : class where TContext : DatabaseContext
    {
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> GetGroup(Expression<Func<TEntity, bool>> predicate);
        TEntity Get(object id);
        TEntity Get(Expression<Func<TEntity, bool>> predicate);
        object Create(TEntity entity);
        void Update(TEntity entity);
        void Delete(object id);
        void Delete(TEntity entity);
    }

    public class Repository<TEntity, TContext> : IRepository<TEntity, TContext> where TEntity : class where TContext : DatabaseContext
    {
        private UnitOfWork<TContext> _unitOfWork;
        public Repository(IUnitOfWork<TContext> unitOfWork)
        {
            _unitOfWork = (UnitOfWork<TContext>)unitOfWork;
        }

        protected ISession Session { get { return _unitOfWork.Session; } }

        public IQueryable<TEntity> GetAll()
        {
            return Session.Query<TEntity>();
        }

        public IQueryable<TEntity> GetGroup(Expression<Func<TEntity, bool>> predicate)
        {
            return Session.Query<TEntity>().Where(predicate);
        }

        public TEntity Get(object id)
        {
            return Session.Get<TEntity>(id);
        }

        public TEntity Get(Expression<Func<TEntity, bool>> predicate)
        {
            return Session.Query<TEntity>().FirstOrDefault(predicate);
        }

        public object Create(TEntity entity)
        {
            return Session.Save(entity);
        }

        public void Update(TEntity entity)
        {
            Session.Update(entity);
        }

        public void Delete(object id)
        {
            Session.Delete(Session.Load<TEntity>(id));
        }

        public void Delete(TEntity entity)
        {
            Session.Delete(entity);
        }
    }
}