﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CustomerPortal.Models
{
    public class InvoiceListItemViewModel
    {
        public double InvoiceAmount { get; set; }
        public DateTime InvoiceDate { get; set; }
        public int InvoiceNumber { get; set; }
        public string PONumber { get; set; }
        public string Status { get; set; }

        public string InvoiceDateJSON
        {
            get
            {
                return InvoiceDate.ToShortDateString();
            }
        }
    }
}