﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CustomerPortal.Models
{
    public class OrderListItemViewModel
    {
        public DateTime OrderDate { get; set; }
        public double OrderValue { get; set; }
        public string PurchaseOrder { get; set; }
        public string SalesOrder { get; set; }
        public string ShipName { get; set; }
        public string Status { get; set; }

        public string OrderDateJSON
        {
            get
            {
                return OrderDate.ToShortDateString();
            }
        }
    }
}