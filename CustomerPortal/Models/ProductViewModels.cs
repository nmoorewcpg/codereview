﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CustomerPortal.Models
{
    public class ProductListItemViewModel
    {
        public string BasePartNumber { get; set; }
        public string CustomerSKU { get; set; }
        public string Description { get; set; }
        [DisplayFormat(DataFormatString = "{0:c}")]
        public double Price { get; set; }
        public string Size { get; set; }
        public string SKU { get; set; }
        public string URL { get; set; }
    }
}