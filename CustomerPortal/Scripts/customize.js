var canvas;
var tshirts = new Array(); //prototype: [{style:'x',color:'white',front:'a',back:'b',price:{tshirt:'12.95',frontPrint:'4.99',backPrint:'4.99',total:'22.47'}}]
var a;
var b;
$(document).ready(function () {
    var line1;
    var line2;
    var line3;
    var line4;

    initDragDrop();

    $('.drawingArea').hover(
        function () {
            var child = $(this).find('.tcanvas')[0];
            $(child).addClass('bordered');
        },
        function () {
            $('.tcanvas').removeClass('bordered');
        }
     );

    $('.tcanvas').each(function (index) {
        var canvasEl = $(this)[0];

        //setup front side canvas 
        canvas = new fabric.Canvas(canvasEl, {
            hoverCursor: 'pointer',
            selection: true,
            selectionBorderColor: 'blue'
        });

        canvasEl.fabric = canvas;

        canvas.on({
            'object:moving': function (e) {
                e.target.opacity = 0.5;
            },
            'object:modified': function (e) {
                e.target.opacity = 1;
            },
            'object:selected': onObjectSelected,
            'selection:cleared': onSelectedCleared
        });

        canvas.findTarget = (function (originalFn) {
            return function () {
                var target = originalFn.apply(this, arguments);
                if (target) {
                    if (this._hoveredTarget !== target) {
                        canvas.fire('object:over', { target: target });
                        if (this._hoveredTarget) {
                            canvas.fire('object:out', { target: this._hoveredTarget });
                        }
                        this._hoveredTarget = target;
                    }
                }
                else if (this._hoveredTarget) {
                    canvas.fire('object:out', { target: this._hoveredTarget });
                    this._hoveredTarget = null;
                }
                return target;
            };
        })(canvas.findTarget);


        canvas.on('mouse:over', function (e) {
            //e.target.setFill('red');
            //canvas.renderAll();
        });

        document.getElementById('remove-selected').onclick = function () {
            var activeObject = canvas.getActiveObject(),
		        activeGroup = canvas.getActiveGroup();
            if (activeObject) {
                canvas.remove(activeObject);
                $("#text-string").val("");
            }
            else if (activeGroup) {
                var objectsInGroup = activeGroup.getObjects();
                canvas.discardActiveGroup();
                objectsInGroup.forEach(function (object) {
                    canvas.remove(object);
                });
            }
        };
        document.getElementById('bring-to-front').onclick = function () {
            var activeObject = canvas.getActiveObject(),
		        activeGroup = canvas.getActiveGroup();
            if (activeObject) {
                activeObject.bringToFront();
            }
            else if (activeGroup) {
                var objectsInGroup = activeGroup.getObjects();
                canvas.discardActiveGroup();
                objectsInGroup.forEach(function (object) {
                    object.bringToFront();
                });
            }
        };
        document.getElementById('send-to-back').onclick = function () {
            var activeObject = canvas.getActiveObject(),
		        activeGroup = canvas.getActiveGroup();
            if (activeObject) {
                activeObject.sendToBack();
            }
            else if (activeGroup) {
                var objectsInGroup = activeGroup.getObjects();
                canvas.discardActiveGroup();
                objectsInGroup.forEach(function (object) {
                    object.sendToBack();
                });
            }
        };

        $('.color-preview').click(function () {
            var color = $(this).css("background-color");
            document.getElementById("shirtDiv").style.backgroundColor = color;
        });

        $('.flip').click(
            function () {
                if ($(this).attr("data-original-title") == "Show Back View") {
                    $(this).attr('data-original-title', 'Show Front View');
                    $('#shirtDiv').css('z-index', 0);
                    $('#shirtBack').css('z-index', 9998);
                    $('.tcanvas').each(function (index) {
                        var thisCanvas = this.fabric;
                        thisCanvas.deactivateAll().renderAll();
                    });
                } else {
                    $(this).attr('data-original-title', 'Show Back View');
                    $('#shirtBack').css('z-index', 0);
                    $('#shirtDiv').css('z-index', 9998);
                    $('.tcanvas').each(function (index) {
                        var thisCanvas = this.fabric;
                        thisCanvas.deactivateAll().renderAll();
                    });
                }
                canvas.renderAll();
                setTimeout(function () {
                    canvas.calcOffset();
                }, 200);
            });
        $(".clearfix button,a").tooltip();
    });
});

function onObjectSelected(e) {
    canvas = e.target.canvas;
    $('.tcanvas').each(function (index) {
        var thisCanvas = this.fabric;
        if (canvas != thisCanvas) {
            thisCanvas.deactivateAll().renderAll();
        }
    });

    var selectedObject = e.target;
    $("#text-string").val("");
    selectedObject.hasRotatingPoint = true

    //display image editor
    $("#imageeditor").css('display', 'block');
}
function onSelectedCleared(e) {
    $("#imageeditor").css('display', 'none');
}
function setFont(font) {
    var activeObject = canvas.getActiveObject();
    if (activeObject && activeObject.type === 'text') {
        activeObject.fontFamily = font;
        canvas.renderAll();
    }
}
function removeWhite() {
    var activeObject = canvas.getActiveObject();
    if (activeObject && activeObject.type === 'image') {
        activeObject.filters[2] = new fabric.Image.filters.RemoveWhite({ hreshold: 100, distance: 10 });//0-255, 0-255
        activeObject.applyFilters(canvas.renderAll.bind(canvas));
    }
}


function handleDragStart(e) {
    [].forEach.call(images, function (img) {
        img.classList.remove('img_dragging');
    });
    this.classList.add('img_dragging');
    $('.tcanvas').addClass('bordered');
}

function handleDragOver(e) {
    if (e.preventDefault) {
        e.preventDefault(); // Necessary. Allows us to drop.
    }

    e.dataTransfer.dropEffect = 'copy'; // See the section on the DataTransfer object.
    // NOTE: comment above refers to the article (see top) -natchiketa

    canvas = $(e.currentTarget).find('.tcanvas')[0].fabric;
    return false;
}

function handleDragEnter(e) {
    // this / e.target is the current hover target.
    this.classList.add('over');
}

function handleDragLeave(e) {
    this.classList.remove('over'); // this / e.target is previous target element.
}

function handleDrop(e) {
    e.preventDefault();
    // this / e.target is current target element.

    if (e.stopPropagation) {
        e.stopPropagation(); // stops the browser from redirecting.
    }

    var img = document.querySelector('#avatarlist img.img_dragging');

    console.log('event: ', e);

    var newImage = new fabric.Image(img, {
        width: img.width,
        height: img.height,
        // Set the center of the new object based on the event coordinates relative
        // to the canvas container.
        left: e.layerX,
        top: e.layerY
    });

    var maxWidth = canvas.getWidth() - (canvas.getWidth() * .1);
    var maxHeight = canvas.getHeight() - (canvas.getHeight() * .1);

    if (img.width >= img.height) {
        newImage.scaleToWidth(maxWidth);
    }
    if (img.height >= img.width) {
        newImage.scaleToHeight(maxHeight);
    }

    canvas.add(newImage);

    return false;
}

function handleDragEnd(e) {
    // this/e.target is the source node.
    [].forEach.call(images, function (img) {
        img.classList.remove('img_dragging');
    });
    $('.tcanvas').removeClass('bordered');
}

var images;
function initDragDrop() {
    if (Modernizr.draganddrop) {
        // Browser supports HTML5 DnD.

        // Bind the event listeners for the image elements
        images = document.querySelectorAll('#avatarlist img');
        [].forEach.call(images, function (img) {
            img.addEventListener('dragstart', handleDragStart, false);
            img.addEventListener('dragend', handleDragEnd, false);
        });
        // Bind the event listeners for the canvas
        var canvasContainers = document.getElementsByClassName('drawingArea');

        for (var i = 0; i < canvasContainers.length; i++) {
            canvasContainers[i].addEventListener('dragenter', handleDragEnter, false);
            canvasContainers[i].addEventListener('dragover', handleDragOver, false);
            canvasContainers[i].addEventListener('dragleave', handleDragLeave, false);
            canvasContainers[i].addEventListener('drop', handleDrop, false);
        }
    } else {
        // Replace with a fallback to a library solution.
        alert("This browser doesn't support the HTML5 Drag and Drop API.");
    }
}