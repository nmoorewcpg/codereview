﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;

namespace SAP.Entities
{
    public class Invoice
    {
        public virtual string InvoiceNumber { get; set; }
        public virtual string InvoiceDateSAP { get; set; }
        public virtual double InvoiceAmount { get; set; }
        public virtual string PONumber { get; set; }
        public virtual string CustomerNumber { get; set; }

        public virtual string Status { get; set; }

        public virtual DateTime InvoiceDate
        {
            get
            {
                return DateTime.ParseExact(InvoiceDateSAP, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
            }
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class OpenInvoice : Invoice
    {
        public override string Status
        {
            get
            {
                return "Open";
            }
        }
    }

    public class PaidInvoice : Invoice
    {
        public override string Status
        {
            get
            {
                return "Paid";
            }
        }
    }

    public sealed class OpenInvoiceMapping : ClassMap<OpenInvoice>
    {
        public OpenInvoiceMapping()
        {
            ReadOnly();
            Id(p => p.InvoiceNumber, "BELNR");
            Map(p => p.InvoiceAmount, "DMBTR");
            Map(p => p.InvoiceDateSAP, "BLDAT");
            Map(m => m.PONumber, "BSTNK_VF");
            Map(m => m.CustomerNumber, "KUNAG");
        }
    }

    public sealed class ClosedInvoiceMapping : ClassMap<PaidInvoice>
    {
        public ClosedInvoiceMapping()
        {
            ReadOnly();
            Id(p => p.InvoiceNumber, "BELNR");
            Map(p => p.InvoiceAmount, "DMBTR");
            Map(p => p.InvoiceDateSAP, "BLDAT");
            Map(m => m.PONumber, "BSTNK_VF");
            Map(m => m.CustomerNumber, "KUNNR");
        }
    }
}